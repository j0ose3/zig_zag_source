﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    public GameObject zigzagPanel;
    public GameObject gameOverPanel;
    public GameObject tapText;
    public TextMeshProUGUI score;
    public Text highScore1;
    public TextMeshProUGUI highScore2;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


	// Use this for initialization
	void Start ()
    {
        highScore1.text = "Highest Score: " + PlayerPrefs.GetInt("highScore").ToString();
    }
	
    public void GameStart()
    {       
        tapText.SetActive(false);
        zigzagPanel.GetComponent<Animator>().Play("PanelUp");
    }

    public void GameOver()
    {
        score.text = PlayerPrefs.GetInt("score").ToString();
        highScore2.text = PlayerPrefs.GetInt("highScore").ToString();

        gameOverPanel.SetActive(true);
    }

    public void Reset ()
    {
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }

	// Update is called once per frame
	void Update ()
    {
        
	}
}
